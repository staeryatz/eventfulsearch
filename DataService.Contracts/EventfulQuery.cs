﻿using System;

namespace DataService.Contracts
{
  /// <summary>
  /// POCO data bag to encapsulate .Net evening data for query.
  /// </summary>
  /// <remarks>
  /// Note that it is not terribly useful to use interface segregation on these
  /// data bag objects, as their only goal is to encapsulate data structures
  /// for passing through .Net eventing. Obviously we could extract an interface
  /// here but it would be highly redundant as the interface would be identical
  /// to the class, minus a constructor. Since the scope of this project does
  /// not include extending or using other services, the interface provides
  /// even less value.
  /// </remarks>
  public class EventfulQuery
  {
    public float Latitude { get; set; }
    public float Longitude { get; set; }

    public string Address { get; private set; }
    public string Category { get; private set; }
    public float Radius { get; private set; }
    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }

    public EventfulQuery(
      string address, string category, float radius,
      DateTime startDate, DateTime endDate)
    {
      Address = address;
      Category = category;
      Radius = radius;
      StartDate = startDate;
      EndDate = endDate;
    }
  }
}
