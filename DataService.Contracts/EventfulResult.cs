﻿using System;
using System.Collections.Generic;

namespace DataService.Contracts
{
  /// <summary>
  /// POCO data bag to encapsulate .Net evening data for query result.
  /// </summary>
  /// <remarks>
  /// Note that it is not terribly useful to use interface segregation on these
  /// data bag objects, as their only goal is to encapsulate data structures
  /// for passing through .Net eventing. Obviously we could extract an interface
  /// here but it would be highly redundant as the interface would be identical
  /// to the class, minus a constructor. Since the scope of this project does
  /// not include extending or using other services, the interface provides
  /// even less value.
  /// 
  /// Also note that the values are read-only as the aren't expected to change
  /// anytime, ever, after originally built from query results.
  /// </remarks>
  public class EventfulResult
  {
    public string Title { get; private set; }
    public string Url { get; private set; }
    public DateTime StartTime { get; private set; }
    public DateTime EndTime { get; private set; }
    public float Price { get; private set; }
    public EventfulVenue Venue { get; private set; }
    public string Image { get; private set; }

    public EventfulResult(
      string title, string url, DateTime startTime, DateTime endTime,
      float price, EventfulVenue venue, string image)
    {
      Title = title;
      Url = url;
      StartTime = startTime;
      EndTime = endTime;
      Price = price;
      Venue = venue;
      Image = image;
    }
  }
}
