﻿using System;

namespace DataService.Contracts
{
  /// <summary>
  /// POCO data bag to encapsulate .Net evening data for query result.
  /// </summary>
  /// <remarks>
  /// Note that it is not terribly useful to use interface segregation on these
  /// data bag objects, as their only goal is to encapsulate data structures
  /// for passing through .Net eventing. Obviously we could extract an interface
  /// here but it would be highly redundant as the interface would be identical
  /// to the class, minus a constructor. Since the scope of this project does
  /// not include extending or using other services, the interface provides
  /// even less value.
  /// 
  /// Also note that the values are read-only as the aren't expected to change
  /// anytime, ever, after originally built from query results.
  /// </remarks>
  public class EventfulVenue
  {
    public string Name { get; private set; }
    public string Url { get; private set; }
    public string Description { get; private set; }
    public string Type { get; private set; }
    public string Address { get; private set; }
    public double Latitude { get; private set; }
    public double Longitude { get; private set; }

    public EventfulVenue(
      string name, string url, string description,
      string type, string address, double latitude, double longitude)
    {
      Name = name;
      Url = url;
      Description = description;
      Type = type;
      Address = address;
      Latitude = latitude;
      Longitude = longitude;
    }
  }
}
