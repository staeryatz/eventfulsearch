﻿using System;

namespace DataService.Contracts
{
  /// <summary>
  /// POCO data bag to encapsulate .Net evening data for query result.
  /// </summary>
  /// <remarks>
  /// Note that it is not terribly useful to use interface segregation on these
  /// data bag objects, as their only goal is to encapsulate data structures
  /// for passing through .Net eventing. Obviously we could extract an interface
  /// here but it would be highly redundant as the interface would be identical
  /// to the class, minus a constructor. Since the scope of this project does
  /// not include extending or using other services, the interface provides
  /// even less value.
  /// 
  /// Also note that the values are read-only as the aren't expected to change
  /// anytime, ever, after originally built from query results.
  /// </remarks>
  public class GeoLocation
  {
    public float Latitude { get; private set; }
    public float Longitude { get; private set; }

    public GeoLocation(float latitude, float longitude)
    {
      Latitude = latitude;
      Longitude = longitude;
    }
  }
}
