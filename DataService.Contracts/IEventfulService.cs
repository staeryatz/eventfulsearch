﻿using System;
using System.Collections.Generic;

namespace DataService.Contracts
{
  public interface IEventfulService
  {
    event EventHandler SearchFailed;
    event EventHandler<IList<EventfulResult>> SearchResultsReturned;
    void PerformSearchAsync(EventfulQuery query);
  }
}
