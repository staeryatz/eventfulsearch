﻿using System;

namespace DataService.Contracts
{
  public interface IGeocodeService
  {
    event EventHandler SearchFailed;
    event EventHandler<GeoLocation> LocationReturned;
    void RequestGeolocationAsync(string address);
  }
}
