﻿using DataService;
using DataService.Contracts;
using DataService.InversionOfControl;
using NUnit.Framework;
using System;

namespace DataService.Tests
{
  using Stubs;

  [TestFixture()]
  public class GeocoderTest
  {
    [Test()]
    public void TestOnlineWithZip()
    {
      // Arrange
      var actualLocation = new GeoLocation(0f, 0f);
      var expectedLocation = new GeoLocation(37.4217550f, -122.0874526f);
      var didLocationReturn = false;

      IGeocodeService geocoder = new GeocodeService();
      geocoder.LocationReturned += (sender, e) =>
      {
        actualLocation = e;
        didLocationReturn = true;
      };

      // Act
      geocoder.RequestGeolocationAsync("94043");

      // Assert
      Assert.IsTrue(didLocationReturn);
      Assert.AreEqual(actualLocation.Latitude, expectedLocation.Latitude);
      Assert.AreEqual(actualLocation.Longitude, expectedLocation.Longitude);
    }

    [Test()]
    public void TestFakeGeocoderWithEmptyString()
    {
      // Arrange
      var didSearchFail = false;
      IGeocodeService geocoder = new FakeGeoCoder();
      geocoder.SearchFailed += (sender, e) => didSearchFail = true;

      // Act
      geocoder.RequestGeolocationAsync(String.Empty);

      // Assert
      Assert.IsTrue(didSearchFail);
    }

    [Test()]
    public void TestFakeGeocoderWithUnknownZip()
    {
      // Arrange
      var didSearchFail = false;
      IGeocodeService geocoder = new FakeGeoCoder();
      geocoder.SearchFailed += (sender, e) => didSearchFail = true;

      // Act
      geocoder.RequestGeolocationAsync("957099");

      // Assert
      Assert.IsTrue(didSearchFail);
    }

    [Test()]
    public void TestFakeGeocoderWithKnownZip()
    {
      // Arrange
      var actualLocation = new GeoLocation(0f, 0f);
      var expectedLocation = new GeoLocation(37.4217550f, -122.0874526f);
      var didLocationReturn = false;

      IGeocodeService geocoder = new FakeGeoCoder();

      geocoder.LocationReturned += (sender, e) =>
      {
        actualLocation = e;
        didLocationReturn = true;
      };

      // Act
      geocoder.RequestGeolocationAsync("94043");

      // Assert
      Assert.IsTrue(didLocationReturn);
      Assert.AreEqual(actualLocation.Latitude, expectedLocation.Latitude);
      Assert.AreEqual(actualLocation.Longitude, expectedLocation.Longitude);
    }

    [Test()]
    public void TestFakeGeocoderWithKnownFullAddress()
    {
      // Arrange
      var actualLocation = new GeoLocation(0f, 0f);
      var expectedLocation = new GeoLocation(37.4217550f, -122.0874526f);
      var didLocationReturn = false;

      IGeocodeService geocoder = new FakeGeoCoder();

      geocoder.LocationReturned += (sender, e) =>
      {
        actualLocation = e;
        didLocationReturn = true;
      };

      // Act
      geocoder.RequestGeolocationAsync(
        "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA");

      // Assert
      Assert.IsTrue(didLocationReturn);
      Assert.AreEqual(actualLocation.Latitude, expectedLocation.Latitude);
      Assert.AreEqual(actualLocation.Longitude, expectedLocation.Longitude);
    }

    [Test()]
    public void TestFakeGeocoderWithKnownCity()
    {
      // Arrange
      var actualLocation = new GeoLocation(0f, 0f);
      var expectedLocation = new GeoLocation(49.246292f, -123.116226f);
      var didLocationReturn = false;

      IGeocodeService geocoder = new FakeGeoCoder();

      geocoder.LocationReturned += (sender, e) =>
      {
        actualLocation = e;
        didLocationReturn = true;
      };

      // Act
      geocoder.RequestGeolocationAsync("Vancouver, BC");

      // Assert
      Assert.IsTrue(didLocationReturn);
      Assert.AreEqual(actualLocation.Latitude, expectedLocation.Latitude);
      Assert.AreEqual(actualLocation.Longitude, expectedLocation.Longitude);
    }
  }
}
