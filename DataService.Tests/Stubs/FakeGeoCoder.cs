﻿using DataService.Contracts;
using System;
using System.Collections.Generic;

namespace DataService.Tests.Stubs
{
  internal sealed class FakeGeoCoder : IGeocodeService
  {
    public event EventHandler SearchFailed;
    public event EventHandler<GeoLocation> LocationReturned;

    private IDictionary<string, GeoLocation> mValidLocations;

    public FakeGeoCoder()
    {
      mValidLocations = new Dictionary<string, GeoLocation>();
      mValidLocations["Vancouver, BC"] = new GeoLocation(49.246292f, -123.116226f);
      mValidLocations["94043"] = new GeoLocation(37.4217550f, -122.0874526f);
      mValidLocations["1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA"]
        = new GeoLocation(37.4217550f, -122.0874526f);
    }

    public void RequestGeolocationAsync(string address)
    {
      if (!mValidLocations.ContainsKey(address))
      {
        OnSearchFailed();
        return;
      }

      OnLocationReturned(mValidLocations[address]);
    }

    private void OnSearchFailed()
    {
      if (SearchFailed != null)
      {
        SearchFailed(this, EventArgs.Empty);
      }
    }

    private void OnLocationReturned(GeoLocation results)
    {
      if (results == null)
      {
        OnSearchFailed();
        return;
      }

      if (LocationReturned != null)
      {
        LocationReturned(this, results);
      }
    }
  }
}
