﻿using RestSharp.Portable;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Xml.Serialization;

namespace DataService
{
  using Contracts;
  using Serialization;

  internal sealed class EventfulService : IEventfulService
  {
    public event EventHandler SearchFailed;
    public event EventHandler<IList<EventfulResult>> SearchResultsReturned;

    public async void PerformSearchAsync(EventfulQuery query)
    {
      // Quoting eventful API docs:
      // Exact ranges can be specified the form 'YYYYMMDD00-YYYYMMDD00',
      // for example '2012042500-2012042700'; the last two digits of each
      // date in this format are ignored.
      var startStr = query.StartDate.ToString(DateFormat);
      var endStr = query.EndDate.ToString(DateFormat);
      var dateRange = String.Format("{0}00-{1}00", startStr, endStr);

      var location = String.Format("{0},{1}", query.Latitude, query.Longitude);

      var client = new RestClient(new Uri(BaseUrl));
      var request = new RestRequest(RequestPath, HttpMethod.Post);
      request.AddParameter("app_key", AppKey);
      request.AddParameter("location", location);
      request.AddParameter("within", query.Radius.ToString());
      request.AddParameter("units", "km");
      request.AddParameter("date", dateRange);
      request.AddParameter("page_size", 50);
      request.AddParameter("category", query.Category);

      var result = await client.Execute<search>(request);
      if (!result.IsSuccess)
      {
        OnSearchFailed ();
        return;
      }

      //string s = System.Text.Encoding.UTF8.GetString(
      //  result.RawBytes, 0, result.RawBytes.Length);
      OnSearchResultsReturned(MarshalResults(result.Data));
    }

    private IList<EventfulResult> MarshalResults(search results)
    {
      var trimmedResults = new List<EventfulResult>();
      foreach (var result in results.events)
      {
        var startTime = DateTime.Now;
        var endTime = startTime.AddDays(1);
        DateTime.TryParse(result.start_time, out startTime);
        DateTime.TryParse(result.stop_time, out endTime);

        var venue = new EventfulVenue(
          result.venue_name, result.venue_url,
          result.description, result.venue_id,
          result.venue_address, result.latitude, result.longitude);

        trimmedResults.Add(new EventfulResult(
          result.title, result.url, startTime, endTime,
          0, venue, result.image.url));
      }
      return trimmedResults;
    }

    private void OnSearchFailed()
    {
      if (SearchFailed != null)
      {
        SearchFailed(this, EventArgs.Empty);
      }
    }

    private void OnSearchResultsReturned(IList<EventfulResult> results)
    {
      if (SearchResultsReturned != null)
      {
        SearchResultsReturned(this, results);
      }
    }

    private const string AppKey = "GjmTbj7sfWkBt4L8";
    private const string BaseUrl = "http://api.eventful.com";
    private const string RequestPath = "rest/events/search";
    private const string DateFormat = "yyyyMMdd";
  }
}
