﻿using RestSharp.Portable;
using System;
using System.Net.Http;
using System.Xml.Serialization;

namespace DataService
{
  using Contracts;
  using Serialization;

  internal sealed class GeocodeService : IGeocodeService
  {
    public event EventHandler SearchFailed;
    public event EventHandler<GeoLocation> LocationReturned;

    public async void RequestGeolocationAsync(string address)
    {
      var client = new RestClient(new Uri(BaseUrl));
      var request = new RestRequest(RequestPath, HttpMethod.Get);
      request.AddParameter("key", AppKey);
      request.AddParameter("address", address);

      var result = await client.Execute<GeocodeResponse>(request);
      if (!result.IsSuccess)
      {
        OnSearchFailed();
        return;
      }

      OnLocationReturned(MarshalResults(result.Data));
    }

    private GeoLocation MarshalResults(GeocodeResponse result)
    {
      if (result.status != StatusOK)
      {
        return null;
      }

      var location = result.result.geometry.location;
      return new GeoLocation(location.lat, location.lng);
    }

    private void OnSearchFailed()
    {
      if (SearchFailed != null)
      {
        SearchFailed(this, EventArgs.Empty);
      }
    }

    private void OnLocationReturned(GeoLocation results)
    {
      if (results == null)
      {
        OnSearchFailed();
        return;
      }

      if (LocationReturned != null)
      {
        LocationReturned(this, results);
      }
    }

    private const string StatusOK = "OK";

    private const string BaseUrl = "https://maps.googleapis.com";
    private const string RequestPath = "maps/api/geocode/xml";

    // This server key only works with requests from IP address 24.86.204.122
    private const string AppKey = "AIzaSyAhemqzNBV__v-qGcj24pWsxy3VY90fykg";

    // This iOS key supposedly should work for iOS Apps with
    // BundeID: com.JeffreyBakker.EventfulSearch, but it always returns
    // a ZERO_RESULTS or REQUEST_DENIED status, depending on Get or Post.
    //private const string AppKey = "AIzaSyBA9Tg5Np8N2ZkggXclv1YTkjwk_nh5jcE";
  }
}
