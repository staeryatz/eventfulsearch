﻿using DataService.Contracts;
using System;
using Ninject;

namespace DataService.InversionOfControl
{
  public class DataServiceModule : Ninject.Modules.NinjectModule
  {
    public override void Load()
    {
      Bind<IEventfulService>().To<EventfulService>().InSingletonScope();
      Bind<IGeocodeService>().To<GeocodeService>().InSingletonScope();
    }
  }
}
