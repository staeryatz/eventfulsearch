﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace DataService.Serialization
{
  // Data bags used to deserialize REST XML results temporarily,
  // to then be marshalled into POCO data bags.

  [DataContract(Namespace = "")]
  internal sealed class image
  {
    [DataMember]
    public string url { get; set; }
  }

  [DataContract(Namespace = "")]
  // Casing really matters for deserialization to work without throwing, but
  // "event" is a C# keyword, so this is the weird exception that allows
  // straying from the XML-defined casing coming from the server.
  internal sealed class Event
  {
    [DataMember]
    [XmlAttribute]
    public string id { get; set; }
    [DataMember]
    public string url { get; set; }
    [DataMember]
    public string title { get; set; }
    [DataMember]
    public string description { get; set; }
    [DataMember]
    public string start_time { get; set; }
    [DataMember]
    public string stop_time { get; set; }
    [DataMember]
    public string venue_id { get; set; }
    [DataMember]
    public string venue_url { get; set; }
    [DataMember]
    public string venue_name { get; set; }
    [DataMember]
    public bool venue_display { get; set; }
    [DataMember]
    public string venue_address { get; set; }
    [DataMember]
    public string city_name { get; set; }
    [DataMember]
    public string region_name { get; set; }
    [DataMember]
    public string region_abbr { get; set; }
    [DataMember]
    public string postal_code { get; set; }
    [DataMember]
    public string country_name { get; set; }
    [DataMember]
    public int all_day { get; set; }
    [DataMember]
    public float latitude { get; set; }
    [DataMember]
    public float longitude { get; set; }
    [DataMember]
    public string geocode_type { get; set; }
    [DataMember]
    public string created { get; set; }
    [DataMember]
    public string owner { get; set; }
    [DataMember]
    public string modified { get; set; }
    [DataMember]
    public image image { get; set; }
  }

  [DataContract(Namespace = "")]
  internal sealed class search
  {
    [DataMember]
    public int total_items { get; set; }
    [DataMember]
    public int page_size { get; set; }
    [DataMember]
    public int page_count { get; set; }
    [DataMember]
    public int page_number { get; set; }
    [DataMember]
    public float search_time { get; set; }
    [DataMember]
    public List<Event> events { get; set; }
  }
}
