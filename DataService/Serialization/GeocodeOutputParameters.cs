﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace DataService.Serialization
{
  // Data bags used to deserialize REST XML results temporarily,
  // to then be marshalled into POCO data bags.

  [DataContract(Namespace = "")]
  internal sealed class location
  {
    [DataMember]
    public float lat { get; set; }

    [DataMember]
    public float lng { get; set; }
  }

  [DataContract(Namespace = "")]
  internal sealed class geometry
  {
    [DataMember]
    public string location_type { get; set; }

    [DataMember]
    public location location { get; set; }
  }

  [DataContract(Namespace = "")]
  internal sealed class result
  {
    [DataMember]
    public string formatted_address { get; set; }

    [DataMember]
    public geometry geometry { get; set; }
  }

  [DataContract(Namespace = "")]
  internal sealed class GeocodeResponse
  {
    [DataMember]
    public string status { get; set; }

    [DataMember]
    public result result { get; set; }
  }
}
