﻿using System;
using DataService.Contracts;
using EventfulSearch.ViewInterfaces;

namespace EventfulSearch.Controllers
{
  public class EventSearchController : IEventSearchController
  {
    private readonly IGeocodeService mGeocodeModel;
    private readonly IEventfulService mEventsModel;
    private readonly ISearchView mSearchView;
    private readonly IResultsView mResultsView;

    private GeoLocation mLatLong;

    public EventSearchController(
      IGeocodeService geoModel, IEventfulService eventsModel,
      ISearchView searchView, IResultsView resultsView)
    {
      mGeocodeModel = geoModel;
      mEventsModel = eventsModel;
      mSearchView = searchView;
      mResultsView = resultsView;

      mEventsModel.SearchResultsReturned += (sender, results) =>
      {
        mResultsView.SetResultsList(results);
        mResultsView.FetchingInProgress = false;
      };

      mGeocodeModel.SearchFailed += (sender, e) =>
        mSearchView.SearchEnabled = false;

      mGeocodeModel.LocationReturned += (sender, location) =>
      {
        mLatLong = location;
        mSearchView.SearchEnabled = true;
      };

      mSearchView.AddressChanged += (sender, address) =>
        mGeocodeModel.RequestGeolocationAsync(address);

      mSearchView.SearchTapped += (sender, query) =>
      {
        query.Latitude = mLatLong.Latitude;
        query.Longitude = mLatLong.Longitude;

        mEventsModel.PerformSearchAsync(query);
        mResultsView.FetchingInProgress = true;
      };
    }
  }
}
