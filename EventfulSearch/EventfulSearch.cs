﻿using System;

using Xamarin.Forms;

using Ninject;
using DataService;
using DataService.Contracts;

namespace EventfulSearch
{
  using Controllers;
  using ViewInterfaces;

  public class App : Application
  {
    public static StandardKernel Container { get; set; }

    public App ()
    {
      // Bootstrap the IoC with modules
      var modules = new Ninject.Modules.NinjectModule[]
      {
        new DataService.InversionOfControl.DataServiceModule(),
        new InversionOfControl.EventfulSearchModule(),
      };

      var kernel = new Ninject.StandardKernel(modules);
      App.Container = kernel;

      var searchPage = kernel.Get<ISearchView>();
      var resultsPage = kernel.Get<IResultsView>();
      kernel.Get<IEventSearchController>();

      var navigationPage = new NavigationPage((ContentPage)searchPage);

      searchPage.SearchTapped += (sender, e) =>
        navigationPage.PushAsync((ContentPage)resultsPage);

      MainPage = navigationPage;
    }

    protected override void OnStart ()
    {
      // Handle when your app starts
    }

    protected override void OnSleep ()
    {
      // Handle when your app sleeps
    }

    protected override void OnResume ()
    {
      // Handle when your app resumes
    }
  }
}
