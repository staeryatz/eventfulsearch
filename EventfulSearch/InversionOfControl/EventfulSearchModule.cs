﻿using System;
using Ninject;

namespace EventfulSearch.InversionOfControl
{
  using Controllers;
  using ViewInterfaces;
  using Views;

  internal sealed class EventfulSearchModule : Ninject.Modules.NinjectModule
  {
    public override void Load()
    {
      Bind<ISearchView>().To<EventfulSearchPage>().InSingletonScope();
      Bind<IResultsView>().To<EventfulResultsPage>().InSingletonScope();
      Bind<IEventSearchController>().To<EventSearchController>().InSingletonScope();
    }
  }
}
