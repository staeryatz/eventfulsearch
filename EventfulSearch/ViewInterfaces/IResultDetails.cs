﻿using System;

namespace EventfulSearch.ViewInterfaces
{
  public interface IResultDetails
  {
    string EventName { set; }
    string EventImage { set; }
    string Venue { set; }
    string Artist { set; }
    DateTime EventDate  { set; }
  }
}
