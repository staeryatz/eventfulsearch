﻿using System;
using System.Collections.Generic;
using DataService.Contracts;

namespace EventfulSearch.ViewInterfaces
{
  public interface IResultsView
  {
    event EventHandler AddToCalendarTapped;

    bool FetchingInProgress { set; }

    void SetResultsList(IList<EventfulResult> results);
  }
}
