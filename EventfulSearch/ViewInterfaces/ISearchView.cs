﻿using System;
using DataService.Contracts;

namespace EventfulSearch.ViewInterfaces
{
  public interface ISearchView
  {
    bool SearchEnabled { get; set; }
    event EventHandler<string> AddressChanged;
    event EventHandler<EventfulQuery> SearchTapped;
    void Reset();
  }
}
