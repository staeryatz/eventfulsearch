﻿using EventfulSearch.ViewInterfaces;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EventfulSearch.Views
{
  public partial class EventItemView : ContentView, IResultDetails
  {
    public string EventName
    {
      set { mTitleLabel.Text = value; }
    }

    public string EventImage
    {
      set { mImage.Source = value; }
    }

    public string Venue
    {
      set { mVenueLabel.Text = value; }
    }

    public string Artist
    {
      set { mArtistLabel.Text = value; }
    }

    public DateTime EventDate
    {
      set { mDateLabel.Text = value.ToString(DateFormat); }
    }

    public EventItemView()
    {
      InitializeComponent ();
    }

    private const string DateFormat = "MM/dd/yyy";
  }
}
