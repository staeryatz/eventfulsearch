﻿using DataService.Contracts;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EventfulSearch
{
  using ViewInterfaces;
  using Views;

  public partial class EventfulResultsPage : ContentPage, IResultsView
  {
    public event EventHandler AddToCalendarTapped;

    public bool FetchingInProgress
    {
      set
      {
        mActivity.IsRunning = mActivity.IsVisible = 
          mActivityLabel.IsVisible = value;
        mResultsDisplayStack.IsVisible = !value;
      }
    }

    public EventfulResultsPage()
    {
      InitializeComponent();

      mAddToCalendarButton.Clicked += OnAddToCalendarTapped;
    }

    public void SetResultsList(IList<EventfulResult> results)
    {
      mResultsStack.Children.Clear();
      mResultsStack.BatchBegin();

      var alternating = false;
      foreach (var result in results)
      {
         var eventItem = new EventItemView
          {
            EventName = result.Title,
            EventImage = result.Image,
            EventDate = result.StartTime,
            Venue = result.Venue.Name,
            //Artist = result.Venue
            // TODO: Get appropriate value
          };
        if (alternating)
        {
          eventItem.BackgroundColor = Color.White.WithLuminosity(0.95);
        }
        mResultsStack.Children.Add(eventItem);
        alternating = !alternating;
      }
      mResultsStack.BatchCommit();
    }

    private void OnAddToCalendarTapped(object sender, EventArgs e)
    {
      if (AddToCalendarTapped != null)
      {
        AddToCalendarTapped(sender, e);
      }
    }
  }
}
