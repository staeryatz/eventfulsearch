﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using DataService.Contracts;

namespace EventfulSearch.Views
{
  using ViewInterfaces;

  public partial class EventfulSearchPage : ContentPage, ISearchView
  {
    public event EventHandler<string> AddressChanged;
    public event EventHandler<EventfulQuery> SearchTapped;

    public bool SearchEnabled
    {
      get { return mSearchButton.IsEnabled; }
      set { mSearchButton.IsEnabled = value; }
    }

    public EventfulSearchPage()
    {
      InitializeComponent();

      mCategoryPicker.Items.Add("Music");
      mCategoryPicker.Items.Add("Sports");
      mCategoryPicker.Items.Add("Performing Arts");

      mStartDatePicker.MinimumDate = DateTime.Now;
      mStartDatePicker.MaximumDate = DateTime.Now.AddDays(MaxDaysAhead);
      mEndDatePicker.MinimumDate = DateTime.Now;
      mEndDatePicker.MaximumDate = DateTime.Now.AddDays(MaxDaysAhead);

      Reset();

      mAddressEntry.Completed += (sender, e) =>
        OnAddressChanged(mAddressEntry.Text);

      mStartDatePicker.DateSelected += (sender, e) =>
      {
        mEndDatePicker.MinimumDate = mStartDatePicker.Date;
        mEndDatePicker.MaximumDate = mStartDatePicker.Date.AddDays(MaxDaysAhead);
      };

      mSearchButton.Clicked += OnSearchTapped;
    }

    public void Reset()
    {
      mAddressEntry.Text = "";
      mRadiusEntry.Text = DefaultRadius.ToString();
      mStartDatePicker.Date = DateTime.Now;
      mEndDatePicker.Date = mStartDatePicker.Date.AddDays(1);

      mCategoryPicker.SelectedIndex = DefaultCategoryIndex;
    }

    private void OnAddressChanged(string address)
    {
      if (String.IsNullOrEmpty (address))
      {
        mSearchButton.IsEnabled = false;
        return;
      }

      if (AddressChanged != null)
      {
        AddressChanged(this, address);
      }
    }

    private void OnSearchTapped(object sender, EventArgs e)
    {
      if (SearchTapped != null)
      {
        var query = ExtractFields();
        SearchTapped(sender, query);
      }
    }

    private EventfulQuery ExtractFields()
    {
      float radius = DefaultRadius;
      float.TryParse(mRadiusEntry.Text, out radius);

      var category = mCategoryPicker.Items[mCategoryPicker.SelectedIndex].
        ToLower().Replace(' ', '_');

      return new EventfulQuery(
        mAddressEntry.Text, category, radius,
        mStartDatePicker.Date, mEndDatePicker.Date);
    }

    private const int MaxDaysAhead = 28;
    private const int DefaultCategoryIndex = 0;
    private const float DefaultRadius = 1.0f;
  }
}
