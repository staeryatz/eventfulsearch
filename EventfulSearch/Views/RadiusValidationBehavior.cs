﻿using System;
using Xamarin.Forms;

namespace EventfulSearch.Views
{
  public class RadiusValidationBehavior : Behavior<Entry>
  {
    protected override void OnAttachedTo(Entry entry)
    {
      entry.TextChanged += OnEntryTextChanged;
      base.OnAttachedTo(entry);
    }

    protected override void OnDetachingFrom(Entry entry)
    {
      entry.TextChanged -= OnEntryTextChanged;
      base.OnDetachingFrom(entry);
    }

    void OnEntryTextChanged(object sender, TextChangedEventArgs args)
    {
      double result;
      bool isDouble = Double.TryParse (args.NewTextValue, out result);
      ((Entry)sender).TextColor = isDouble ? Color.Default : Color.Red;

      if (!isDouble)
      {
        return;
      }

      if (result < Minimum)
      {
        ((Entry)sender).Text = Minimum.ToString();
      }
      else if (result > Maximum)
      {
        ((Entry)sender).Text = Maximum.ToString();
      }
    }

    private const int Minimum = 0;
    private const int Maximum = 300;
  }
}
