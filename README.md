# Introduction #

This repo contains a Xamarin.Forms application (currently targeting iPhone) which uses the eventful.com REST API to search for upcoming events local to a geographic location, using user defined search criteria. This was created and run in Xamarin Studio with a Xamarin.iOS Business Edition license.

### Design ###

The design incorporates SOLID design principles for low coupling, the MVC pattern to separate logic, data store and view, and Command Query Responsibility Segregation to allow for asynchronous data requests. The solution structure also separates projects, interfaces and concrete implementations from each other. Below are the projects inside the solution:

 * DataService.Contracts - Contains interfaces for the Eventful and Geocode services, as well as definitions of POCO objects which act as data envelopes for messaging to client of the data service.

 * DataService - Contains concrete implementations of the Eventful and Geocode services, XML serialization definitions for the returned REST data, marshalling of said data to POCO objects, and an Inversion of Control module for registering the abstraction of DataService types into the container.

 * EventfulSearch - Contains a cross-platform client application, which uses the DataService. Registers its view abstractions into the IoC container and resolves DataService dependencies. Contains an MVC search controller object to co-ordinate view and data. Also contains View classes, which ideally would belong in a EventfulSearch.UI project, and its interfaces in an EventfulSearch.Contracts project - but the current should be sufficient design for such a small application. Were other subsystems to come into place, this would be a different story.

* EventfulSearch.iOS - This is a boilerplate Xamarin.Forms project responsible for executing on iOS and launching the Xamarin application.

* EventfulSearch.UITests - This includes automated UI tests to simulate user input. It currently has very basic validation.

* DataService.Tests - Simple unit tests against the components inside the project. There is not much to test, since we are not here to test rest services or libraries, which have themselves been tested, and the fact that the majority of the logic in this application is around user input validation (which can be tested at the UI automation level). There should have been a EventfulSearch.Tests to host tests for the EventSearchController, which co-ordinates much of the work between the two REST services, as well as the user.

Note that all the projects and code are PCL (with the exception of EventfulSearch.iOS), so they should run on any supported platform. The UI is generic enough that it should also work on Android.

### Caveats ###

Since this project is using Google Geocode server key (the API link was for static geocoding), the results would only return correctly if run from my specific IP address. I have generated an iOS app key for the Geocode API to be called from my specific Bundle ID, but it always results in request denied. It is possible to go in the code and change the key (in [GeoCodeService.cs](https://bitbucket.org/staeryatz/eventfulsearch/src/96a2541e6bd11e32168bb534c81a8d6265ae4ccb/DataService/GeocodeService.cs?at=master&fileviewer=file-view-default)) to a known working one to try the application out.

Here are some images of the application running successfully:

![ev_01.png](https://bitbucket.org/repo/rb49Rk/images/87296985-ev_01.png)

![ev_02.png](https://bitbucket.org/repo/rb49Rk/images/1567179079-ev_02.png)

![ev_03.png](https://bitbucket.org/repo/rb49Rk/images/3546591572-ev_03.png)

![ev_04.png](https://bitbucket.org/repo/rb49Rk/images/513660068-ev_04.png)

![ev_05.png](https://bitbucket.org/repo/rb49Rk/images/229004541-ev_05.png)

### Acknowledgements ###

The following open source libraries were used:

 * [RestSharp.Portable](https://github.com/FubarDevelopment/restsharp.portable) - a PCL-compatible adaptation of the RestSharp C# API for REST. 
 * [Ninject for Portable Class Libraries](https://github.com/onovotny/ninject/) - a PCL-compatible version of the Ninject Inversion of Control container.

### License ###
Copyright (C)2015 Jeffrey Bakker. All rights reserved.
Released under the MIT license (see LICENSE.md for full text).