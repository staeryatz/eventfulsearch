﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;

namespace EventfulSearch.UITests
{
  [TestFixture]
  public class Tests
  {
    iOSApp app;

    [SetUp]
    public void BeforeEachTest ()
    {
      app = ConfigureApp.iOS.StartApp ();
    }

    [Test]
    public void ApplicationTitleTextIsDisplayed ()
    {
      // Arrange/Act
      AppResult[] results = app.WaitForElement (c => c.Marked ("Eventful Search"));
      // Assert
      Assert.IsTrue (results.Any ());
    }

    [Test]
    public void DefaultRadiusValueIs1 ()
    {
      // Arrange/Act
      var radius = app.Query (c => c.Marked("RadiusField")).FirstOrDefault();

      // Assert
      Assert.IsTrue (radius.Text == "1");
    }

    [Test]
    public void DefaultCategoryValueIsMusic ()
    {
      // Arrange/Act
      var cat = app.Query (c => c.Marked("CategoryField")).FirstOrDefault();

      // Assert
      Assert.IsTrue (cat.Text == "Music");
    }


    [Test]
    public void DefaultStartDateValueIsToday ()
    {
      // Arrange
      var expected = DateTime.Now.Date;

      // Act
      var date = app.Query (c => c.Marked("StartDateField")).FirstOrDefault();

      // Assert
      var actual = DateTime.Parse (date.Text).Date;
      Assert.AreEqual (expected, actual);
    }

    [Test]
    public void DefaultEndDateValueIsTomorrow ()
    {
      // Arrange
      var expected = DateTime.Now.AddDays(1).Date;

      // Act
      var date = app.Query (c => c.Marked("EndDateField")).FirstOrDefault();

      // Assert
      var actual = DateTime.Parse (date.Text).Date;
      Assert.AreEqual (expected, actual);
    }

    [Test]
    public void SearchDisabledOnStartup ()
    {
      // Arrange/Act
      var search = app.Query (c => c.Button("Search")).FirstOrDefault();
      // Assert
      Assert.IsFalse (search.Enabled);
    }

    [Test]
    public void SearchEnabledWithZipCode ()
    {
      // Arrange/Act
      app.EnterText(c => c.Marked("AddressField"), "94043");
      app.Tap(c => c.Button("Done"));

      // Assert
      WaitFor(4);
      var search = app.Query (c => c.Button("Search")).FirstOrDefault();

      // Assert
      Assert.IsTrue (search.Enabled);
    }

    [Test]
    public void SearchEnabledWithFullAddress ()
    {
      // Arrange/Act
      app.EnterText(c => c.Marked("AddressField"),
        "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA");
      app.Tap(c => c.Button("Done"));

      // Assert
      WaitFor(4);
      var search = app.Query (c => c.Button("Search")).FirstOrDefault();

      // Assert
      Assert.IsTrue (search.Enabled);
    }

    private void WaitFor(double seconds)
    {
      var waitUntil = DateTime.Now.AddSeconds(seconds);
      app.WaitFor (() =>
        {
          if (DateTime.Now >= waitUntil)
          {
            return true;
          }
          return false;
        });
    }
  }
}
